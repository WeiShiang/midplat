package com.example.midplat.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface ContactPersonService {
	public List<Map<String, Object>> listContactInfo(String divIss, String billTo, String subDiv, int pageNum, int pageSize) throws SQLException;
}
