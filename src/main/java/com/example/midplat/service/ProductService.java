package com.example.midplat.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface ProductService {
	public void listItemNo(String itemNo) throws SQLException;
	public void getProductPrice(String co, String itemNo) throws SQLException;
}