package com.example.midplat.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerProfileDTO {
	
	private String name;
	private String unifyNo;
	private String creditFlag;
	private String nameE;
	private String acctType;
	private String docFlag;
	
	public CustomerProfileDTO() {
		super();
	}

	public CustomerProfileDTO(String name, String unifyNo, String creditFlag, String nameE, String acctType,
			String docFlag) {
		super();
		this.name = name;
		this.unifyNo = unifyNo;
		this.creditFlag = creditFlag;
		this.nameE = nameE;
		this.acctType = acctType;
		this.docFlag = docFlag;
	}

	@Schema(example = "好命公司", description = "公司名稱")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Schema(example = "01234567", description = "統一編號")
	public String getUnifyNo() {
		return unifyNo;
	}

	public void setUnifyNo(String unifyNo) {
		this.unifyNo = unifyNo;
	}

	@Schema(example = "B", description = "B:代表已註冊B2B")
	public String getCreditFlag() {
		return creditFlag;
	}

	public void setCreditFlag(String creditFlag) {
		this.creditFlag = creditFlag;
	}
	
	@Schema(example = "GoodLife", description = "公司英文名稱")
	public String getNameE() {
		return nameE;
	}

	public void setNameE(String nameE) {
		this.nameE = nameE;
	}
	
	@Schema(example = "*", description = "客戶別。  *:代表鎖檔")
	public String getAcctType() {
		return acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
	@Schema(example = "C", description = "交易級別")
	public String getDocFlag() {
		return docFlag;
	}

	public void setDocFlag(String docFlag) {
		this.docFlag = docFlag;
	}
	
}
