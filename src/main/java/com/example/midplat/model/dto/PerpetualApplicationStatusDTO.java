package com.example.midplat.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.example.midplat.model.bo.PerpetualApplicationStatusBO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PerpetualApplicationStatusDTO {

	List<PerpetualApplicationStatusBO> cpList;

	public PerpetualApplicationStatusDTO() {
		super();
	}

	public PerpetualApplicationStatusDTO(List<PerpetualApplicationStatusBO> cpList) {
		super();
		this.cpList = cpList;
	}

	public List<PerpetualApplicationStatusBO> getCpList() {
		return cpList;
	}

	public void setCpList(List<PerpetualApplicationStatusBO> cpList) {
		this.cpList = cpList;
	}
	
	
	
}
