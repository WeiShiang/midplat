package com.example.midplat.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.v3.oas.annotations.media.Schema;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductPriceDTO {

	private String itemNo;
	private String begDate;
	private String endDate;
	private String userUp;
	private String maxUp;
	private String minUp;
	private String dlyCost;

	public ProductPriceDTO() {
		super();
	}

	public ProductPriceDTO(String itemNo, String begDate, String endDate, String userUp, String maxUp, 
			               String minUp, String dlyCost) {
		super();
		this.itemNo = itemNo;
		this.begDate = begDate;
		this.endDate = endDate;
		this.userUp = userUp;
		this.maxUp = maxUp;
		this.minUp = minUp;
		this.dlyCost = dlyCost;
	}

	@Schema(example = "12342377790", description = "展碁料號")
	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	@Schema(example = "2021-10-01", description = "價格起始日期 ")
	public String getBegDate() {
		return begDate;
	}

	public void setBegDate(String begDate) {
		this.begDate = begDate;
	}
	
	@Schema(example = "2021-12-31", description = "價格截止日期 ")
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	@Schema(example = "620", description = "建議售價")
	public String getUserUp() {
		return userUp;
	}

	public void setUserUp(String userUp) {
		this.userUp = userUp;
	}
	
	@Schema(example = "580", description = "最高經銷價")
	public String getMaxUp() {
		return maxUp;
	}

	public void setMaxUp(String maxUp) {
		this.maxUp = maxUp;
	}

	@Schema(example = "575", description = "最低經銷價")
	public String getMinUp() {
		return minUp;
	}

	public void setMinUp(String minUp) {
		this.minUp = minUp;
	}
	
	@Schema(example = "550", description = "PM管理成本")
	public String getDlyCost() {
		return dlyCost;
	}

	public void setDlyCost(String dlyCost) {
		this.dlyCost = dlyCost;
	}

}
