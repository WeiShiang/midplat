package com.example.midplat.model.bo;

import io.swagger.v3.oas.annotations.media.Schema;

public class SpecBO {
	private int seq;
	private String specName;
	private String specValue;

	public SpecBO() {
		super();
	}

	public SpecBO(int seq, String specName, String specValue) {
		super();
		this.seq = seq;
		this.specName = specName;
		this.specValue = specValue;
	}
	
	@Schema(example = "1", description = "順序")
	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}
	
	@Schema(example = "產品名稱", description = "規格名稱")
	public String getSpecName() {
		return specName;
	}

	public void setSpecName(String specName) {
		this.specName = specName;
	}
	
	@Schema(example = "小米手錶運動版", description = "規格內容 ")
	public String getSpecValue() {
		return specValue;
	}

	public void setSpecValue(String specValue) {
		this.specValue = specValue;
	}

}
