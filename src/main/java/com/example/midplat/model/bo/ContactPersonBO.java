package com.example.midplat.model.bo;

import io.swagger.v3.oas.annotations.media.Schema;

public class ContactPersonBO {

	private String subDiv;
	private String isEmpNo;
	private String isName;
	private String isEmail;
	private String smEmpNo;
	private String smName;
	private String smEmail;
	private String b2bFlag;

	public ContactPersonBO() {
		super();
	}

	public ContactPersonBO(String subDiv, String isEmpNo, String isName, String isEmail, String smEmpNo, String smName,
			String smEmail, String b2bFlag) {
		super();
		this.subDiv = subDiv;
		this.isEmpNo = isEmpNo;
		this.isName = isName;
		this.isEmail = isEmail;
		this.smEmpNo = smEmpNo;
		this.smName = smName;
		this.smEmail = smEmail;
		this.b2bFlag = b2bFlag;
	}

	@Schema(example = "YS0", description = "部門")
	public String getSubDiv() {
		return subDiv;
	}

	public void setSubDiv(String subDiv) {
		this.subDiv = subDiv;
	}

	@Schema(example = "", description = "內勤工號")
	public String getIsEmpNo() {
		return isEmpNo;
	}

	public void setIsEmpNo(String isEmpNo) {
		this.isEmpNo = isEmpNo;
	}

	@Schema(example = "張O水", description = "內勤姓名")
	public String getIsName() {
		return isName;
	}

	public void setIsName(String isName) {
		this.isName = isName;
	}

	@Schema(example = "water@weblink.com.tw", description = "內勤email")
	public String getIsEmail() {
		return isEmail;
	}

	public void setIsEmail(String isEmail) {
		this.isEmail = isEmail;
	}

	@Schema(example = "周O星", description = "外勤工號")
	public String getSmEmpNo() {
		return smEmpNo;
	}

	public void setSmEmpNo(String smEmpNo) {
		this.smEmpNo = smEmpNo;
	}

	@Schema(example = "", description = "外勤姓名")
	public String getSmName() {
		return smName;
	}

	public void setSmName(String smName) {
		this.smName = smName;
	}

	@Schema(example = "star@weblink.com.tw", description = "外勤email")
	public String getSmEmail() {
		return smEmail;
	}

	public void setSmEmail(String smEmail) {
		this.smEmail = smEmail;
	}

	@Schema(example = "N", description = "B2B註記。 Y:B2B業績所屬內外勤 ")
	public String getB2bFlag() {
		return b2bFlag;
	}

	public void setB2bFlag(String b2bFlag) {
		this.b2bFlag = b2bFlag;
	}

}
