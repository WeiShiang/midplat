package com.example.midplat.model.bo;

import java.sql.Date;

public class CustomerProfileBO {
	private String billTo;
	private String address;
	private String areaCode;
	private String tel;
	private String extNo;
	private String name;
	private String contB;
	private String contS;
	private String contA;
	private String unifyNo;
	private String shipCnt;
	private String paymentType;
	private Short paymentDate;
	private Short dueDays;
	private String payment;
	private String arGlno;
	private String creditFlag;
	private String custArea;
	private String acctType;
	private String taxType;
	private Integer stock;
	private String bank;
	private Date date;
	private String telex;
	private String fax;
	private String eMail;
	private String addrCode;
	private String openTime;
	private String closeTime;
	private String nameE;
	private String addrE;
	private String attn;
	private String continent;
	private String country;
	private String abbr;
	private String desti;
	private String forwarder;
	private String applyDiv;
	private String creditNote;
	private String payType;
	private Short uPayDate;
	private String docFlag;
	private String tradeTerm;
	private Date createDate;
	private Date lastTnsDate;
	
	public CustomerProfileBO() {
		super();
	}
	
	public String getBillTo() {
		return billTo;
	}
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getExtNo() {
		return extNo;
	}
	public void setExtNo(String extNo) {
		this.extNo = extNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContB() {
		return contB;
	}
	public void setContB(String contB) {
		this.contB = contB;
	}
	public String getContS() {
		return contS;
	}
	public void setContS(String contS) {
		this.contS = contS;
	}
	public String getContA() {
		return contA;
	}
	public void setContA(String contA) {
		this.contA = contA;
	}
	public String getUnifyNo() {
		return unifyNo;
	}
	public void setUnifyNo(String unifyNo) {
		this.unifyNo = unifyNo;
	}
	public String getShipCnt() {
		return shipCnt;
	}
	public void setShipCnt(String shipCnt) {
		this.shipCnt = shipCnt;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Short getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Short paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Short getDueDays() {
		return dueDays;
	}
	public void setDueDays(Short dueDays) {
		this.dueDays = dueDays;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getArGlno() {
		return arGlno;
	}
	public void setArGlno(String arGlno) {
		this.arGlno = arGlno;
	}
	public String getCreditFlag() {
		return creditFlag;
	}
	public void setCreditFlag(String creditFlag) {
		this.creditFlag = creditFlag;
	}
	public String getCustArea() {
		return custArea;
	}
	public void setCustArea(String custArea) {
		this.custArea = custArea;
	}
	public String getAcctType() {
		return acctType;
	}
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	public String getTaxType() {
		return taxType;
	}
	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTelex() {
		return telex;
	}
	public void setTelex(String telex) {
		this.telex = telex;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	public String getAddrCode() {
		return addrCode;
	}
	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}
	public String getOpenTime() {
		return openTime;
	}
	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}
	public String getCloseTime() {
		return closeTime;
	}
	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}
	public String getNameE() {
		return nameE;
	}
	public void setNameE(String nameE) {
		this.nameE = nameE;
	}
	public String getAddrE() {
		return addrE;
	}
	public void setAddrE(String addrE) {
		this.addrE = addrE;
	}
	public String getAttn() {
		return attn;
	}
	public void setAttn(String attn) {
		this.attn = attn;
	}
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAbbr() {
		return abbr;
	}
	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}
	public String getDesti() {
		return desti;
	}
	public void setDesti(String desti) {
		this.desti = desti;
	}
	public String getForwarder() {
		return forwarder;
	}
	public void setForwarder(String forwarder) {
		this.forwarder = forwarder;
	}
	public String getApplyDiv() {
		return applyDiv;
	}
	public void setApplyDiv(String applyDiv) {
		this.applyDiv = applyDiv;
	}
	public String getCreditNote() {
		return creditNote;
	}
	public void setCreditNote(String creditNote) {
		this.creditNote = creditNote;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public Short getuPayDate() {
		return uPayDate;
	}
	public void setuPayDate(Short uPayDate) {
		this.uPayDate = uPayDate;
	}
	public String getDocFlag() {
		return docFlag;
	}
	public void setDocFlag(String docFlag) {
		this.docFlag = docFlag;
	}
	public String getTradeTerm() {
		return tradeTerm;
	}
	public void setTradeTerm(String tradeTerm) {
		this.tradeTerm = tradeTerm;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getLastTnsDate() {
		return lastTnsDate;
	}
	public void setLastTnsDate(Date lastTnsDate) {
		this.lastTnsDate = lastTnsDate;
	}
	
}
