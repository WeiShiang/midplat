package com.example.midplat.model.bo;

import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

public class ProductInfoBO {
	private String itemNo;
	private List<SpecBO> specList;

	public ProductInfoBO() {
		super();
	}

	public ProductInfoBO(String itemNo, List<SpecBO> specList) {
		super();
		this.itemNo = itemNo;
		this.specList = specList;
	}
	
	@Schema(example = "12342377790", description = "料號")
	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public List<SpecBO> getSpecList() {
		return specList;
	}

	public void setSpecList(List<SpecBO> specList) {
		this.specList = specList;
	}

}
