package com.example.midplat.controller;

import javax.validation.constraints.Size;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.midplat.logic.CustomerLogic;
import com.example.midplat.model.dto.CustomerProfileDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "customer", description = "the customer API")
public class CustomerController {
	
	private static Logger log = LogManager.getLogger(CustomerController.class);
	
	@Autowired 
	private CustomerLogic customerLogic;
	
	@GetMapping(value = "/customers/{customerID}/profile", produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "get customer profile by bill_to"))
	public CustomerProfileDTO getCustomerProfile(@PathVariable(value = "customerID") @Size(min = 7) String billTo) {
		return customerLogic.getCustomerProfile(billTo);
	}
	
}
