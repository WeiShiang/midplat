package com.example.midplat.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.midplat.model.dto.ContactPersonDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "empolyee", description = "the empolyee API")
public class EmpolyeeContorller {
	
	private static Logger log = LogManager.getLogger(ContactPersonController.class);
	
	@GetMapping(value = {"/fellow-worker"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "The service is available"))
	public ContactPersonDTO listSameDeptEmpNo(	@RequestParam(required=false) String empNo) {
		log.error("receive request empNo={}", empNo);
		return new ContactPersonDTO();
	}
}
