package com.example.midplat.controller;


import java.sql.SQLException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.midplat.logic.ContactPersonLogic;
import com.example.midplat.model.dto.ContactPersonDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name = "contact person", description = "the contact person API")
public class ContactPersonController {
	
	private static Logger log = LogManager.getLogger(ContactPersonController.class);
	
	@Autowired
	private ContactPersonLogic contactPersonLogic;
	
	@GetMapping(value = {"/contacts/{customerID}"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "")
	@ApiResponses(value = @ApiResponse(responseCode = "200", description = "The service is available"))
	public ContactPersonDTO listContactPersonInfo(	@PathVariable(value = "customerID") String billTo, 
													@RequestParam(required=false) String devision,
													@RequestParam(required=false, defaultValue = "1") int pageNum,
													@RequestParam(required=false, defaultValue = "500") int pageSize) {
		log.error("receive request billTo={}, devision={}, pageNum={}, pageSzie={}", billTo, devision, pageNum, pageSize);
		ContactPersonDTO responseDTO = null;
		try {
			responseDTO = contactPersonLogic.listContactInfo(billTo, devision, pageNum, pageSize);
		} catch (SQLException e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}		
		return responseDTO;
	}
	

	
}
