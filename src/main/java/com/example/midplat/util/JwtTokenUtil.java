package com.example.midplat.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;

import javax.security.auth.message.AuthException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenUtil {
	
	private static Logger log = LogManager.getLogger(JwtTokenUtil.class);
	
	public static void main(String[] args)  {

//	產生金鑰組 Generate the key pair (public key and private key).
//	KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
//	指定金鑰長度2048-bit   Specify that the key should be 2048-bit.
//	keygen.initialize(2048);  //若拿掉random就不會每執行一次就變一次金鑰
//	KeyPair generatedKeyPair = keygen.generateKeyPair();
//	aveKeyPair(PATH, generatedKeyPair);
	   try {
		   String path = "D:\\example\\cert\\";
		   String algorithm = "RSA";
		   KeyPair k =loadKeyPair(path, "public.key" , "private.key",algorithm);
		   PublicKey  publicKey = k.getPublic();
		   PrivateKey privateKey = k.getPrivate();
		   String jwt = generateToken(privateKey);
		   System.out.println(jwt);
//	   	   System.out.println(validateToken(jwt));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param pubkey
	 * @param jwt
	 * @return
	 */
	public static Claims validateToken(PublicKey pubkey, String jwt) throws AuthException {
		Claims c = null;
		try {
			c = Jwts.parserBuilder()
					.setSigningKey(pubkey)
					.build()
					.parseClaimsJws(jwt)
					.getBody();
		}  catch (MalformedJwtException e) {
			throw new AuthException("Invalid JWT token.");
		} catch (ExpiredJwtException e) {
			throw new AuthException("Expired JWT token");
		} catch (UnsupportedJwtException e) {
			throw new AuthException("Unsupported JWT token");
		} catch (IllegalArgumentException e) {
			throw new AuthException("JWT token compact of handler are invalid");
		} catch (JwtException  e) {
			throw new AuthException("JWT token are invalid");
		}
		return c;
	}
	
	/**
	 * generate JWT for signature
	 * @param privateKey
	 * @return
	 */
	public static String generateToken(PrivateKey privateKey) {
		return Jwts.builder()
				   .setIssuer("example")
//				   .setSubject(subject)
//				   .setAudience("testAudienceId")
				   .setExpiration(Date.from(LocalDateTime.now().plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant())) // now + 10分鐘
				   .setIssuedAt(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()))
//				   .setId("testuserid")
				   .signWith(privateKey)
				   .compact();
	}
	
	/**
	 * 	讀取金鑰 Load the keys from files.
	 * @param path
	 * @param algorithm
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws java.io.IOException 
	 */
	public static KeyPair loadKeyPair(String path, String publicKeyName, String privateKeyName, String algorithm) throws NoSuchAlgorithmException, InvalidKeySpecException, java.io.IOException {

		// Initiate the factory with specified algorithm.
		KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
		// Read public key from file.
		File fileForPublicKey = Paths.get(path, publicKeyName).toFile();

		FileInputStream publicFis = new FileInputStream(fileForPublicKey);
		byte[] loadedPublicBytes = new byte[(int) fileForPublicKey.length()];
		publicFis.read(loadedPublicBytes);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(loadedPublicBytes);
		PublicKey publicKey = keyFactory.generatePublic(spec);
		
		// Read private key from file.
		File fileForPrivateKey = Paths.get(path, "private.key").toFile();
		//log.trace("Private key will be loaded from '{}'.", fileForPrivateKey);
		FileInputStream privateFis = new FileInputStream(fileForPrivateKey);
		byte[] loadedPrivateBytes = new byte[(int) fileForPrivateKey.length()];
		privateFis.read(loadedPrivateBytes);
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(loadedPrivateBytes);
		PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
	
		return new KeyPair(publicKey, privateKey);
	}
	
	/**
	 * 	讀取金鑰 Load the PublicKey from files.
	 * @param path
	 * @param algorithm
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws java.io.IOException 
	 */
	public static PublicKey loadPublicKey(String path, String fileName,  String algorithm) throws NoSuchAlgorithmException, InvalidKeySpecException, java.io.IOException {

		// Initiate the factory with specified algorithm.
		KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
		// Read public key from file.
		File fileForPublicKey = Paths.get(path, fileName).toFile();

		FileInputStream publicFis = new FileInputStream(fileForPublicKey);
		byte[] loadedPublicBytes = new byte[(int) fileForPublicKey.length()];
		publicFis.read(loadedPublicBytes);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(loadedPublicBytes);
		return keyFactory.generatePublic(spec);
	}
	
	/**
	 * 讀取金鑰  Load the PrivateKey from files.
	 * @param path
	 * @param algorithm
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws java.io.IOException 
	 */
	public static PrivateKey loadPrivateKey(String path, String fileName, String algorithm) throws NoSuchAlgorithmException, InvalidKeySpecException, java.io.IOException {

		// Initiate the factory with specified algorithm.
		KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
		
		// Read private key from file.
		File fileForPrivateKey = Paths.get(path, fileName).toFile();
		//log.trace("Private key will be loaded from '{}'.", fileForPrivateKey);
		FileInputStream privateFis = new FileInputStream(fileForPrivateKey);
		byte[] loadedPrivateBytes = new byte[(int) fileForPrivateKey.length()];
		privateFis.read(loadedPrivateBytes);
		PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(loadedPrivateBytes);
		return keyFactory.generatePrivate(privateKeySpec);
	}
	
	/**
	 * 	印出金鑰以確認金鑰相同 
	 *  Print the loaded key pair to ensure that they are exactly the same.
	 * @param keyPair
	 */
	 public static void printKeyPair(KeyPair keyPair) {
	        Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
	        System.out.println ("public key:  " + Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
	        Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());
	        System.out.println ("private key: " + Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()) );
	  }
	 
	 /**
	  *	儲存金鑰  Store the key as files.
	  * @param path
	  * @param keyPair
	  * @throws IOException
	  * @throws FileNotFoundException
	  * @throws java.io.IOException
	  */
	public static void saveKeyPair(String path, KeyPair keyPair) throws IOException, FileNotFoundException, java.io.IOException {
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();

		// Store Public Key.
		File fileForPublicKey = Paths.get(path, "public.key").toFile();
		// log.trace("Public key will be output to '{}'.", fileForPublicKey);

		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(publicKey.getEncoded());
		FileOutputStream publicFos = new FileOutputStream(fileForPublicKey);
		publicFos.write(x509EncodedKeySpec.getEncoded());

		// Store Private Key.
		File fileForPrivateKey = Paths.get(path, "private.key").toFile();

		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
		FileOutputStream privateFos = new FileOutputStream(fileForPrivateKey);
		privateFos.write(pkcs8EncodedKeySpec.getEncoded());
	}
	

}
