package com.example.midplat.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	/**
	 * util.Date to String
	 * 
	 * @param pattern
	 * @param date
	 * @return
	 */
	public static String date2String(String pattern, Date date) {
		SimpleDateFormat sdFormat = new SimpleDateFormat(pattern);
		return sdFormat.format(date);
	}
	
	
	
}
