package com.example.midplat.logic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.example.midplat.service.ContactPersonService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.example.midplat.model.bo.ContactPersonBO;
import com.example.midplat.model.dto.ContactPersonDTO;

@Component
public class ContactPersonLogic {
	private static Logger log = LogManager.getLogger(ContactPersonLogic.class);
	
	@Autowired
	private ContactPersonService contactPersonService;
	
	/**
	 * 格式化回傳資料
	 * @param page
	 * @param dataBOList
	 * @return
	 */
	 private ContactPersonDTO setRtnMessage(PageInfo page, List dataBOList) {
		ContactPersonDTO rtnObj = new ContactPersonDTO();
		rtnObj.setCurrentPage(page.getPageNum());
		rtnObj.setPages(page.getPages());
		rtnObj.setPageSize(page.getPageSize());
		rtnObj.setTotalCount(((Long)page.getTotal()).intValue());
		rtnObj.setItems(dataBOList);
		return rtnObj;
	}
	
	public ContactPersonDTO listContactInfo(String billTo, String devision, int pageNum, int pageSize) throws SQLException{
		List<Map<String, Object>> list = contactPersonService.listContactInfo("", billTo, devision, pageNum, pageSize);
		List<ContactPersonBO> rtnList = null;
		ContactPersonDTO response = null;
		if (CollectionUtils.isNotEmpty(list)) {
			rtnList = new ArrayList<ContactPersonBO>();
			for (Map<String, Object> row: list) {
				rtnList.add(new ContactPersonBO());
			}
			response = this.setRtnMessage(new PageInfo(list), rtnList);
		}
		return response;
	}
	
	
	
}
