package com.example.midplat.logic;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import com.example.midplat.service.ProductService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.midplat.model.dto.ProductInfoDTO;
import com.example.midplat.model.dto.ProductPriceDTO;
import com.example.midplat.util.DateUtil;

@Component
public class ProductLogic {
	
	private static Logger log = LogManager.getLogger(ProductLogic.class);
	
	@Autowired
	private ProductService productService;
	
	/**
	 * 
	 * @param itemNo
	 * @return
	 * @throws SQLException
	 */
	public ProductInfoDTO getProductInfo(String itemNo) throws SQLException{
		ProductInfoDTO productDTO = new ProductInfoDTO();
		try {
			productService.listItemNo(itemNo);

		} catch (SQLException e) {
			log.error(ExceptionUtils.getStackTrace(e));
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}
		return productDTO;
	}
	
	/**
	 * 
	 * @param itemNo
	 * @return
	 * @throws SQLException
	 */
	public ProductPriceDTO getProductPrice(String co, String itemNo) throws SQLException{
		ProductPriceDTO productDTO = null;
		try {
			productService.getProductPrice(co, itemNo);
//			if (Objects.nonNull(productEntity)) {
//				String item = productEntity.getItemNo();
//				String begDate = DateUtil.date2String("yyyy-MM-dd", productEntity.getBegDate());
//				String endDate = DateUtil.date2String("yyyy-MM-dd", productEntity.getEndDate());
//				String usrUp = productEntity.getUsrUp().setScale(0, BigDecimal.ROUND_HALF_UP).toString();
//				String maxUp = productEntity.getMaxUp().setScale(0, BigDecimal.ROUND_HALF_UP).toString();
//				String minUp = productEntity.getMinUp().setScale(0, BigDecimal.ROUND_HALF_UP).toString();
//				String dlyCost = productEntity.getDlyCost().setScale(0, BigDecimal.ROUND_HALF_UP).toString();
//				productDTO = new ProductPriceDTO(item, begDate, endDate, usrUp, maxUp,
//												 minUp, dlyCost);
//			}
		} catch (SQLException e) {
			log.error(ExceptionUtils.getStackTrace(e));
		} catch (Exception e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}
		return productDTO;
	}
	
}
