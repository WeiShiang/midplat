package com.example.midplat.logic;

import java.sql.SQLException;

import com.example.midplat.service.CustomerService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import com.example.midplat.dao.sec.entity.ErrcodedEntity;
import com.example.midplat.model.dto.CustomerProfileDTO;

@Component
public class CustomerLogic {
	
	private static Logger log = LogManager.getLogger(CustomerLogic.class);
	
	@Autowired
	private CustomerService custService;
	
	public CustomerProfileDTO getCustomerProfile(String billTo){
		CustomerProfileDTO rtnBO = null;
		try {
			Gson gson = new Gson();
			//entity to BO
			String rtnJson = gson.toJson(custService.getCustomerProfile(billTo));
			rtnBO = gson.fromJson(rtnJson, CustomerProfileDTO.class);
			
		} catch (SQLException e) {
			log.error(ExceptionUtils.getStackTrace(e));
		}
		return rtnBO;
	}

	/**
	 * @Transactional 僅能針對一個datasource使用
	 * @throws SQLException
	 */
//	@Transactional(isolation = Isolation.DEFAULT, rollbackFor = {SQLException.class })
	@Transactional(value = "secondaryTransactionManager", isolation = Isolation.DEFAULT, rollbackFor = {SQLException.class })
	public void testTX() throws SQLException {
//		ErrcodeEntity codeEntity = new ErrcodeEntity();
//		codeEntity.setCodeType("00");
//		codeEntity.setCodeNo("04");
//		codeEntity.setCodeNoDesc("test2");
//		codeEntity.setCreateDiv("test2");
//		codeEntity.setModDate("2021-09-09");
//		int i0 = testService.delete(codeEntity);
//		log.debug("i0=" + i0 );
//		int i = testService.insert(codeEntity);
//		log.debug("i=" + i );
//		
//		codeEntity.setCodeNoDesc("test2");
//		codeEntity.setCreateDiv("test2");
//		codeEntity.setModDate("2021-09-15");
//		int i2  = testService.update(codeEntity);
//		log.debug("i2=" + i2);
		
//		ErrcodedEntity coded01Entity1 = new ErrcodedEntity();
//		coded01Entity1.setCodeType("00");
//		coded01Entity1.setCodeNo("04");
//		coded01Entity1.setCodeNoDesc("test2");
//		coded01Entity1.setCreateDiv("test2");
//		coded01Entity1.setModDate("2021-09-09");
//		
//		coded01Service.insert(coded01Entity1);
//		throw new  SQLException();
		
		 // 開始分頁，必須緊跟著mapper 
		log.debug("====================================");
		PageHelper.startPage(1, 10);
//        List<ErrcodedEntity> list2 = coded01Service.findAllByCodeType();
//		PageInfo page2 = new PageInfo(list2);
//		list2.stream().forEach(x -> log.debug(x.getCodeNo()));
//		log.debug(page2.getTotal());
	}
	
	
	
	
}
