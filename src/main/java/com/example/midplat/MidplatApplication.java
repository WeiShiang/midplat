package com.example.midplat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MidplatApplication {

	public static void main(String[] args) {
		SpringApplication.run(MidplatApplication.class, args);
	}
}
