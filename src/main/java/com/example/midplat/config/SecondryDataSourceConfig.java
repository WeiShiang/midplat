package com.example.midplat.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(basePackages = "com.example.midplat.dao.sec.mapper", sqlSessionFactoryRef = "secondarySessionFactory")
public class SecondryDataSourceConfig {

	private static Logger log = LogManager.getLogger(SecondryDataSourceConfig.class);
	
	// From the configuration file, get the relevant configuration of the database
	@Bean(name = "secondaryDataSourceProperties")
	@ConfigurationProperties(prefix = "spring.datasource.secondary")
	public DataSourceProperties secondaryDataSourceProperties() {
		return new DataSourceProperties();
	}

	// Instance creation of DataSource
	@Bean(name = "secondaryDataSource")
	public DataSource secondaryDataSource(@Qualifier("secondaryDataSourceProperties") DataSourceProperties secondaryDataSourceProperties) {
		final HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName(secondaryDataSourceProperties.getDriverClassName());
		hikariConfig.setJdbcUrl(secondaryDataSourceProperties.getUrl());
		hikariConfig.setUsername(secondaryDataSourceProperties.getUsername());
		hikariConfig.setPassword(secondaryDataSourceProperties.getPassword());
		hikariConfig.setMinimumIdle(3);
		hikariConfig.setMaximumPoolSize(10);
		hikariConfig.setConnectionTimeout(30000);
		hikariConfig.setIdleTimeout(60000);
		hikariConfig.setConnectionTestQuery("SELECT 1");
		return new HikariDataSource(hikariConfig);
	}

	@Bean(name = "secondarySessionFactory")
	public SqlSessionFactory secondarySessionFactory(@Qualifier("secondaryDataSource") DataSource dataSource)
			throws Exception {
		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		bean.setDataSource(dataSource);
		bean.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/secondary/*.xml"));
		return bean.getObject();
	}

	@Bean(name = "secondaryTransactionManager")
	public DataSourceTransactionManager secondaryTransactionManager(
			@Qualifier("secondaryDataSource") DataSource dataSource) {
		return new DataSourceTransactionManager(dataSource);
	}

	@Bean(name = "secondarySessionTemplate")
	public SqlSessionTemplate secondarySessionTemplate(
			@Qualifier("secondarySessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

}
