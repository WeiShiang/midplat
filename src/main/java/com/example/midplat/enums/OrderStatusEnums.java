package com.example.midplat.enums;

public enum OrderStatusEnums {
	PROCESS("process"),
	APPROVED("approved"), 
	DECLINED("declined") 
	;
	private String value;

	OrderStatusEnums(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return String.valueOf(value);
	}
	
	public static OrderStatusEnums fromValue(String value) {
		for (OrderStatusEnums b : OrderStatusEnums.values()) {
			if (b.value.equals(value)) {
				return b;
			}
		}
		throw new IllegalArgumentException("Unexpected value '" + value + "'");
	}
}
