package com.example.midplat.dao.pri.entity;

public class ErrcodeEntity {

	private String codeType;
	private String codeNo;
	private String codeNoDesc;
	private String createDiv;
	private String modDate;

	public ErrcodeEntity() {
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCodeNo() {
		return codeNo;
	}

	public void setCodeNo(String codeNo) {
		this.codeNo = codeNo;
	}

	public String getCodeNoDesc() {
		return codeNoDesc;
	}

	public void setCodeNoDesc(String codeNoDesc) {
		this.codeNoDesc = codeNoDesc;
	}

	public String getCreateDiv() {
		return createDiv;
	}

	public void setCreateDiv(String createDiv) {
		this.createDiv = createDiv;
	}

	public String getModDate() {
		return modDate;
	}

	public void setModDate(String modDate) {
		this.modDate = modDate;
	}
	
	
}
