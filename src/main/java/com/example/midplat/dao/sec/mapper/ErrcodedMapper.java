package com.example.midplat.dao.sec.mapper;

import java.util.List;

import com.example.midplat.dao.sec.entity.ErrcodedEntity;

public interface ErrcodedMapper {

	public List<ErrcodedEntity> selectByPrimaryKey();
	public int insert(ErrcodedEntity entity);
	public int update(ErrcodedEntity entity);
	public int delete(ErrcodedEntity entity);
	public List<ErrcodedEntity> findAllByCodeType();
}
